
window.onload = () => {
  modal();
  pickDate();
  // stickyMenu();
  sliderBanner();
  sliderPrimary();
  sliderSecondary();
  sliderTertiary();
  sliderSelect();
  sliderBackground();
  minHeightOffset('.js-min-height-offset');
  minHeightOffset('body');
  togglePlaceSearch();
  toggleSwitch();
  clearInput('.input-search');
  otp();
  countdownOtp();
}

window.onresize = () => {
  minHeightOffset('.js-min-height-offset'); 
  minHeightOffset('body');
}

function modal() {
  let $modal = $('.modal');
  if ($modal.length > 0) {
    $modal.on('shown.bs.modal', e => {
      const $backdrop = $('.modal-backdrop');
      $backdrop.click(e => $modal.modal('hide') );
    });
  }

  let $modalPickSeat = $('.js-modal-pick-seat');
  if ($modalPickSeat.length > 0) {
    const buttonSubmit = $modalPickSeat.find('.button-submit')[0];
    $(buttonSubmit).click(e => {
      const self    = $(e.currentTarget);
      const control = self.attr('data-control');
      const modal   = $(self).closest('.modal');
      const value   = $(modal).find('.radio-box.active input')[0].value;
      
      if ($(control).length > 0) {
        if ($(control).prop('tagname') == 'INPUT') {
          $(control).value(value);
        } else {
          const controlInput = $(control).find('input')[0];
          if (controlInput) controlInput.value = value;
        }
      }
    });

    $modalPickSeat.on('show.bs.modal', e => {
      const control      = $(e.relatedTarget);
      const value        = $(control).find('input')[0].value;
      const modal        = $(e.currentTarget);
      const optionActive = $(modal).find('input[value="' + value + '"]')[0];

      if (optionActive) {
        $(modal).find('.btn').button('dispose');
        $(optionActive).closest('.btn').button('toggle');
      }
    });
  }

  let $close = $('.modal .button-close');
  $close.click(e => {
    e.preventDefault;
    const modal = $(e.currentTarget).closest('.modal');
    if (modal) $(modal).modal('hide');
  });
}

function toggleElement() {
  let btnToggle = $('[data-toggle="toggle-element"]');
  btnToggle.click(e => {
    e.preventDefault;
    const target = $(e.currentTarget).attr('data-target');
    const targetEl = $(target);
    if (targetEl.length > 0) {
      targetEl.toggle(200);
    }
  });
}

function sliderBanner() {
  if ($('.slider-banner').length > 0 ) {
    var swiper = new Swiper('.slider-banner .swiper-container', {
      slidesPerView: 1,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      effect: 'fade'
    });
  }
}

function sliderPrimary() {
  if ($('.slider-primary').length > 0 ) {
    var swiper = new Swiper('.slider-primary .swiper-container', {
      slidesPerView: 1.5,
      spaceBetween: 16,
      fadeEffect: {
        crossFade: true
      },
      breakpoints: {
        576: {
          slidesPerView: 2
        }
      },
    });
  }
}

function sliderSelect() {
  let sliderSelect = $(".slider-select .swiper-container");
  if (sliderSelect.length > 0) {
    sliderSelect.each(function (index, element) {
      var swiper = new Swiper(element, {
        direction: "vertical",
        slidesPerView: 3,
        centeredSlides: true,
      });
      $(".modal").on("shown.bs.modal", function (e) {
        swiper.update();
      });
    });

    $('.modal#modal-pick-customer .modal-footer .button-submit').click(function (e) { 
      e.preventDefault();
      let adultNum = $('.slider-select.select-adult .swiper-slide-active').text(),
        childrenNum = $('.slider-select.select-child .swiper-slide-active').text(),
        babyNum = $('.slider-select.select-baby .swiper-slide-active').text(),
        customerInput = $('.input-group[data-target="#modal-pick-customer"] .input-control');
      console.log(customerInput);
      customerInput.val(`${adultNum} người lớn, ${childrenNum} trẻ em, ${babyNum} em bé`);
    });
  }
}

function sliderSecondary() {
  var swiper = new Swiper('.slider-secondary .swiper-container', {
    slidesPerView: 2.2,
    spaceBetween: 16,
    breakpoints: {
      576: {
        slidesPerView: 2
      }
    },
    fadeEffect: {
      crossFade: true
    }
  });
}

function sliderTertiary() {
  var swiper = new Swiper('.slider-tertiary .swiper-container', {
    slidesPerView: 1,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  });
}

function sliderBackground() {
  var swiper = new Swiper('.slider-background .swiper-container', {
    slidesPerView: 1,
    loop: true,
    effect: 'fade',
    autoplay: {
      delay: 7000
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  });
}

function stickyMenu() {
  const navbar = document.querySelector('header .navbar-top');
  if (navbar) {
    const headroom = new Headroom(navbar);
    headroom.init();
  }
}

function minHeightOffset(el) {
  let $el = $(el);
  if ($el.length > 0) {
    const windowHeight = window.innerHeight;
    const navBotHeight = $('.navbar-bottom').length > 0 ? $('.navbar-bottom').height() : 0;
    $el.each((index, element) => {
      const offset = $(element).offset();
      $(element).css({
        'min-height': (windowHeight - navBotHeight - offset.top) + 'px'
      });
    })
  } 
}

function togglePlaceSearch() {
  const area = $('.js-toggle-place');
  if (area.length > 0) {
    const buttonToggle = $(area).find('.button')[0];
    const targetDepart = $(buttonToggle).attr('data-depart');
    const targetArrived = $(buttonToggle).attr('data-arrived');
    
    if ($(targetDepart).length > 0 && $(targetArrived).length > 0) {
      $(buttonToggle).click(e => {
        e.preventDefault();
        const icon = $(e.currentTarget).find('[class^="ic"]')[0];
        toggleValue($(targetDepart), $(targetArrived));
        $(icon).addClass('animating');

        setTimeout(() => $(icon).removeClass('animating'), 300);
      });
    }
  }

  function toggleValue(el1, el2) {
    const $input1 = $(el1).find('.input-control')[0];
    const $input2 = $(el2).find('.input-control')[0];
    const suffix1 = $(el1).find('.input-suffix')[0];
    const suffix2 = $(el2).find('.input-suffix')[0];

    let tValue = $input1.value;
    let tSuffix = suffix1.innerText;

    $input1.value = $input2.value;
    suffix1.innerText = suffix2.innerText;

    $input2.value = tValue;
    suffix2.innerText = tSuffix;

    return false;
  }
}

function toggleSwitch() {
  // $().button('toggle');
  let toggle = $('.js-toggle-switch');

  if (toggle.length > 0) {
    const toggleControl    = $(toggle).find('.toggle-control')[0];
    let items              = $(toggle).find('.toggle-item label');
    let currentActive = $(toggle).find('.toggle-item.active')[0];

    animateToggleControl($(currentActive).position().left, $(currentActive).width() - 4);
    _toggleTarget($(currentActive).find('label')[0]);

    $(items).click(e => {
      const self  = e.currentTarget;
      const left  = $(self).position().left - 4;
      
      animateToggleControl(left, $(self).width());
      _toggleTarget(self);
    });

    function animateToggleControl(position, width) {
      $(toggleControl).css({
        transform: 'translateX(' + (position) + 'px)',
        width: width
      });
    }

    // internal functions
    function _toggleTarget(el) {
      const target = $(el).attr('data-target');
      const targetExpanded = $(el).attr('aria-expanded');
      const targetAdaptive = $(el).attr('data-target-adaptive');

      if ($(target).length > 0 && targetExpanded != undefined) {
        targetExpanded === 'true' ? targetShow() : targetHide();
      }

      function targetShow() {
        $(target).show();
        $(targetAdaptive).removeClass('adaptive');
        $(targetAdaptive).attr('data-target', '#modal-pick-date-round-trip')
      }
  
      function targetHide() {
        $(target).hide();
        $(targetAdaptive).addClass('adaptive');
        $(targetAdaptive).attr('data-target', '#modal-pick-date-one-way')
      }
    }
  }
}

function pickDate() {
  const $daterangeOneWay    = $('#date-booking-flight-one-way');
  const $daterangeRoundTrip = $('#date-booking-flight-round-trip');
  const $inputDepart        = $('#input-box-depart');
  const $inputArrived       = $('#input-box-arrived');
  const labelDepart         = $inputDepart.length > 0 ? $inputDepart.find('.input-label')[0].innerText : 'Ngày đi';
  const labelArrived        = $inputArrived.length > 0 ? $inputArrived.find('.input-label')[0].innerText : 'Ngày về';
  const priceByDay          = window.priceByDay;
  const today               = moment().date() + '/' + (moment().month() + 1) + '/' + moment().year();
  let options = {
    inline: true,
    alwaysOpen: true,
    singleMonth: true,
    customArrowPrevSymbol: '<span class="ic-arrow-left-circle"></span>',
    customArrowNextSymbol: '<span class="ic-arrow-right-circle"></span>',
    startOfWeek: 'monday',
    showTopbar: false,
    format: 'DD-MM-YYYY',
    showShortcuts: false,
    language: 'auto',
    showDateFilter: function(time, date) {
      const t = new Date(time);
      const d = t.getDate();
      const m = t.getMonth() + 1;
      const y = t.getFullYear();

      const group = priceByDay ? priceByDay.filter(group => group.month == m && group.year == y) : null;
      const priceDate = group && group.length > 0 ? group[0].prices.filter(obj => obj.date == d) : null;
      const price = priceDate && priceDate.length > 0 ? priceDate[0].price : '-';

      return `
        <span>${date}</span>
        <div class="day-price">${price}</div>
      `;
    },
    
  };

  // Pick one way
  if ($daterangeOneWay.length > 0) {
    const startDate = $inputDepart.length > 0 ? $inputDepart.find('input')[0].value : today;
    let optionsOneWay = {
      ...options,
      singleDate : true,
      container: '#date-booking-flight-one-way',
      setValue: function(s) {
        _setValueDepart(s);
        _closeModal(this);
      },
      getValue: () => startDate,
    }
    $daterangeOneWay.dateRangePicker(optionsOneWay)
    .bind('datepicker-change', (e, obj) => {
      const newStartDate   = moment(obj.value, 'DD-MM-YYYY');
      const input          = $inputArrived.find('input')[0];
      const currentEndDate = moment(input.value, 'DD-MM-YYYY');

      $daterangeRoundTrip.data('dateRangePicker').setStart(obj.value);
      if (newStartDate.isAfter(currentEndDate)) {
        $daterangeRoundTrip.data('dateRangePicker').setEnd(obj.value);
        _setValueArrived(obj.value);
      } 
    });
  }
  
  // Pick round trip
  if ($daterangeRoundTrip.length > 0) {
    const startDate = $inputDepart.length > 0 ? $inputDepart.find('input')[0].value : today;
    const endDate = $inputArrived.length > 0 ? $inputArrived.find('input')[0].value : '';
    let optionsRoundTrip = {
      ...options,
      separator : ' ',
      container: '#date-booking-flight-round-trip',
      setValue: function(s) {
        const times = s.split(' ');
        _setValueDepart(times[0]);
        _setValueArrived(times[1]);
        _closeModal(this);
      },
      getValue: () => startDate + ' ' + endDate,
    }
    $daterangeRoundTrip.dateRangePicker(optionsRoundTrip)
    .bind('datepicker-first-date-selected', (e, obj) => {
      const $modal = $(e.currentTarget).closest('.modal');
      if ($modal && labelArrived.length > 0) $modal.find('.modal-title')[0].innerText = labelArrived;
    })
    .bind('datepicker-change', (e, obj) => {
      const $modal = $(e.currentTarget).closest('.modal');
      // Will change after 1s when modal closed
      setTimeout(() => {
        if ($modal && labelDepart.length > 0) $modal.find('.modal-title')[0].innerText = labelDepart;
      }, 1000);
    });
  }

  // Update value when open modal
  // Not best solution but cannot update on event of round trip. Because cannot click range after update.
  const $modalOneWay = $('#modal-pick-date-one-way');
  if ( $modalOneWay.length > 0 && $.contains($modalOneWay[0],$daterangeOneWay[0]) ) {
    $modalOneWay.on('show.bs.modal', e => {
      const time = $inputDepart.find('input')[0].value;
      $daterangeOneWay.data('dateRangePicker').setStart(time);
    });
  }

  // internal functions
  function _setValueDepart(time) {
    if ($inputDepart.length > 0) {
      $inputDepart.find('.input-text')[0].innerText = time;
      $inputDepart.find('input')[0].value = time;
    }
  }
  function _setValueArrived(time) {
    if ($inputArrived.length > 0) {
      $inputArrived.find('.input-text')[0].innerText = time;
      $inputArrived.find('input')[0].value = time;
    }
  }
  function _closeModal(datePicker) {
    const modal = $(datePicker).closest('.modal');
    if (modal) {
      $(modal).modal('hide');
    }
  }
}

function clearInput(inputGroup) {
  if ($(inputGroup).length > 0) {
    $(inputGroup).each((index, group) => {
      const delButton = $(group).find('.delete')[0];
      const input = $(group).find('input');
      $(input).on('keyup', e => {
        let value = e.currentTarget.value;
        let areaResult = $(e.currentTarget).attr('area-results');
        let areaSuggestion = $(e.currentTarget).attr('area-suggestion');

        if (value.length > 0) {
          $(delButton).show();
          if ($(areaResult).length > 0) $(areaResult).show();
          if ($(areaSuggestion).length > 0) $(areaSuggestion).hide();
        } else {
          $(delButton).hide();
          if ($(areaResult).length > 0) $(areaResult).hide();
          if ($(areaSuggestion).length > 0) $(areaSuggestion).show();
        }
      });
      $(delButton).click(e => {
        e.preventDefault();
        $(input).val('');
        $(e.currentTarget).hide();
        let areaResult = $(input).attr('area-results');
        let areaSuggestion = $(input).attr('area-suggestion');
        if ($(areaResult).length > 0) $(areaResult).hide();
        if ($(areaSuggestion).length > 0) $(areaSuggestion).show();
      });
    });
  }
}

function otp() {
  if ($('.digit-group').length > 0) {
    // $('.digit-group input').each(function() {
    //   $(this).attr('maxlength', 1);
    //   $(this).on('keyup', function(e) {
    //     var parent = $($(this).parent());
        
    //     if(e.keyCode === 8 || e.keyCode === 37) {
    //       var prev = parent.find('input#' + $(this).data('previous'));
          
    //       if(prev.length) {
    //         $(prev).select();
    //       }
    //     } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
    //       var next = parent.find('input#' + $(this).data('next'));
          
    //       if(next.length) {
    //         $(next).select();
    //       } else {
    //         if(parent.data('autosubmit')) {
    //           parent.submit();
    //         }
    //       }
    //     }
    //   });
    // });
    $('.digit-group input').on('keydown', function(e) {
      e.preventDefault();
      var parent = $($(this).parent());
      
      // Delete number
      if (e.keyCode === 8 || e.keyCode === 127) {
        if ($(this).val() === "") {
          var prev = parent.find('input#' + $(this).data('previous'));
          
          if (prev.length) {
            $(prev).trigger('select');
          }
        } else {
          $(this).val("");
        }
      } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) {
        $(this).val(e.key);
        var next = parent.find('input#' + $(this).data('next'));
        
        if (next.length) {
          $(next).trigger('select');
        } else {
          if (parent.data('autosubmit')) {
            parent.submit();
          }
        }
      }
    });
  }
}

function countdownOtp() {
  if ($('.section-otp .button-countdown .countdown-timer').length > 0) {
    var i = 30;
    let countdown = setInterval(() => {
      if (i > 0) {
        i--;
        $('.section-otp .button-countdown .countdown-timer').text((i >= 10) ? `0:${i}` : `0:0${i}`);
      } else {
        clearInterval(countdown);
      }
    }, 1000)
  }
}
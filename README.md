# 8Trip mobile website - FE

## Plugins required
- Node >=8
- Gulp 4

## How to work?
1. Install
```npm install``` or ```yarn```
2. Run project
```gulp```

### Show modal
- Add class `active` for `.modal`
- Edit content modal in `app/views/components/_modal.pug`

### Show loading button
- Add class `.is-loading` for `button`

## Form status
When click button, add class `.is-loading` for form to stop pointer events and show status.
var gulp        = require('gulp');
var sass        = require('gulp-sass');
var pug         = require('gulp-pug');
var babel       = require('gulp-babel');
var concat      = require('gulp-concat');
var uglify      = require('gulp-uglify');
var rename      = require('gulp-rename');
var cleanCSS    = require('gulp-clean-css');
var del         = require('del');
var browserSync = require('browser-sync').create();
var sourcemaps  = require('gulp-sourcemaps');
var imagemin    = require('gulp-imagemin');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var plumber = require('gulp-plumber');

const paths = {
  html: {
    src: 'app/views/**/*.pug',
    ignore: '!app/views/**/_*.pug',
    dest: 'dist'
  },
  styles: {
    src: 'app/sass/**/*.scss',
    ignore: '!app/sass/**/_*.scss',
    dest: 'dist/assets/css'
  },
  scripts: {
    src: 'app/js/**/*.js',
    dest: 'dist/assets/js'
  },
  images: {
    src: 'app/images/**/*',
    dest: 'dist/assets/images'
  },
  dist: 'dist',
  vendors: {
    src: [
      'app/vendors/jquery.min.js',
      'app/vendors/moment.min.js',
      'app/vendors/*.js'
    ],
    dest: 'dist/assets/js'
  },
  fonts: {
    src: 'app/fonts/**/*',
    dest: 'dist/assets/fonts'
  }
}

const clean = () => del([ paths.dist ]);

function html() {
  return gulp.src([paths.html.src, paths.html.ignore])
    .pipe(plumber())
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(paths.html.dest))
    .pipe(browserSync.stream());
}

function styles() {
  var plugins = [
    autoprefixer()
  ];
  return gulp.src(paths.styles.src)
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(cleanCSS())
    .pipe(rename({
      basename: 'styles',
      suffix: '.min'
    }))
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(babel())
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest(paths.scripts.dest))
    .pipe(browserSync.stream());
}

function vendors() {
  if (paths.vendors.src.length > 0) {
    return gulp.src(paths.vendors.src)
      .pipe(uglify())
      .pipe(concat('vendors.min.js'))
      .pipe(gulp.dest(paths.vendors.dest))
      .pipe(browserSync.stream());
  }
}

function images() {
  return gulp.src(paths.images.src)
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {cleanupIDs: false}
        ]
      })
    ]))
    .pipe(gulp.dest(paths.images.dest))
    .pipe(browserSync.stream());
}

function vendors() {
  if (paths.vendors.src.length > 0) {
    return gulp.src(paths.vendors.src)
      .pipe(uglify())
      .pipe(concat('vendors.min.js'))
      .pipe(gulp.dest(paths.vendors.dest))
      .pipe(browserSync.stream());
  }
}

function fonts() {
  return gulp.src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest));
}

function serve() {
  browserSync.init({
    server: {
      baseDir: paths.dist,
    },
    port: 8080,
    ui: {
      port: 8081
    }
  })

  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.src, styles);
  gulp.watch(paths.html.src, html);
  gulp.watch(paths.images.src, images);
}

function defaultTask(cb) {
  cb();
  serve();
}

var build = gulp.series(clean, gulp.parallel(fonts, vendors, html, styles, scripts, images));

exports.default = gulp.series(defaultTask, build);
exports.clean   = clean;
exports.build   = build;
exports.images  = images;
exports.vendors = vendors;
exports.html    = html;
exports.styles  = styles;
exports.scripts = scripts;
